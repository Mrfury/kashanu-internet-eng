from http.server import BaseHTTPRequestHandler
from urllib import parse


class GetHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        parsed_path = parse.urlparse(self.path)
        myfile = open('index.html',encoding='utf-8')
        # print(myfile)
        message_parts = myfile.readlines()
        # print(message_parts)
        message = '\r\n'.join(message_parts)
        # print(message)
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(bytes(message.encode('utf8')))
        myfile.close()
        # self.send_response(200)


        # self.wfile.write(message.encoding('utf8'))


if __name__ == '__main__':
    from http.server import HTTPServer
    server = HTTPServer(('localhost', 1500), GetHandler)
    print('Starting server, use <Ctrl-C> to stop')
    server.serve_forever()
